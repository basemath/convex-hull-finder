/**
 * @author David Bogle
 * @author John Rankin
 */

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MergeHull implements ConvexHullFinder
{
    
    // Constants
    public static final int TOP    = 0;
    public static final int BOTTOM = 1;
    public static final int RIGHT  = 2;
    public static final int LEFT   = 3;
    
    /**
     * This method is an implementation of computeHull from
     * the ConvexHullFinder interface.  It uses the mergehull
     * algorithm to computer the hull.
     * 
     * @param listOfPoints a List of Point2D around which the
     * hull is being computed.
     * @return a list of Point2D that composes the hull CCW
     */
    
    public List<Point2D> computeHull( List<Point2D> points )
    {       
    	// First Step is to Sort the points by the X Value
        Collections.sort( points, new ComparePoint() );
    	        
        return recursiveMergeHull( points );
    }

    
    private List<Point2D> recursiveMergeHull( List<Point2D> points ) {
        
    	// Local Variable
        List<Point2D> returnList = new ArrayList<Point2D>();
        
        // The base case is a set of points less than 4. This is
        // because a set of 3 points will always include all the
        // points in the convex hull.
        if( points.size() < 4 ) {
        	
        	// This returns the base case with the points in Counter Clockwise Order.
            returnList = ccw( points );
        }
        else {
        	// Split the incoming points into two equal sets, left and right.
            // The incoming points are assumed to be sorted.           
            int midPoint = points.size() / 2;
            List<Point2D> leftPoints = points.subList( 0, midPoint );
            List<Point2D> rightPoints = points.subList( midPoint, points.size() );
            
            
            // Merge the results of the left hull and the right hull
            returnList = mergeHulls( recursiveMergeHull(leftPoints),
                                     recursiveMergeHull(rightPoints) );
        }
        
        // Return List
        return returnList;
    }

    private List<Point2D> mergeHulls( List<Point2D> left, List<Point2D> right ) {
    // For Merge Hull, once the all of the points and been separated down to the base case,
    // we have essentially 3 steps:
    // 
    // Step 1: First, find the leftmost point on the right hull and
    //         the rightmost point on the left hull, and use those
    //         points to create the starting tangent.
    // Step 2: Create the upper Tangent Lines from both Hulls:
    // Step 3: Build the the new Convex Hull
    	
    	// Step1:
        Point2D pointA = rightMostPoint( left );        
        Point2D pointB = leftMostPoint( right );
        Line2D startingLine = new Line2D.Double( pointA, pointB );
        
        // Step 2:
        Line2D upperTangent = walkUp( startingLine, left, right );
        Line2D lowerTangent = walkDown( startingLine, left, right );
        
        // Step 3:
        return buildHull( left, right, upperTangent, lowerTangent );
    }
    
    private List<Point2D> buildHull( List<Point2D> left, List<Point2D> right, Line2D upperTangent, Line2D lowerTangent ) {
    // When building the hull, CCW order is extremely important.
    // The build pattern goes in a circular motion.
    //
    // Step 1: Add the Upper-Right Tangent Point
    // Step 2: Add the Upper-Left Tangent Point
    // Step 3: Find the index of the UpperLeft Tangent point, and the LowerLeft Tangent point
    //         and traverse through the loop and add the points to the list
    // Step 4: Add the Lower-Left Tangent point
    // Step 5: Add the Lower-Right Tangent point
    // Step 6: Find the index of the Lower-Right Tangent point, and the Upper-Right Tangent point
    //         and traverse through the loop and add the points to the list
    // 
    // Note: We use contains() to make sure that no duplicate points are added to the list.
    	
    	List<Point2D> returnList = new ArrayList<Point2D>();
    	
    	// Step 1:
    	addToHull( upperTangent.getP2(), returnList );
    	
    	// Step 2:
    	addToHull( upperTangent.getP1(), returnList );
        
    	// Step 3:
        int currentIndex = left.indexOf(upperTangent.getP1());
        int lastIndex  = left.indexOf(lowerTangent.getP1());
        
        // Traverse
        while( currentIndex != lastIndex ) {
        	currentIndex = indexAbove( currentIndex, left , LEFT );
        	addToHull( left.get(currentIndex), returnList );
        }
        
        // Step 4:
    	addToHull( lowerTangent.getP1(), returnList );
        
        // Step 5:
    	addToHull( lowerTangent.getP2(), returnList );
        
        // Step 6:
        currentIndex = right.indexOf(lowerTangent.getP2());
        lastIndex  = right.indexOf(upperTangent.getP2());
        		
        // Traverse
        while( currentIndex != lastIndex ) {
        	currentIndex = indexBelow( currentIndex, right , RIGHT );
        	addToHull( right.get(currentIndex), returnList );
        }
        
        // Return List
        return returnList;
    }
    
    private void addToHull( Point2D point, List<Point2D> hull ) {
	// This method checks to see if a duplicate point has already
    // been added to the hull, and if not, adds the given point to
    // the hull.  It was implemented as a method to factor out
    // redundant code.
    	
    	if( !hull.contains( point ) ){
    		hull.add(point);
    	}
    }
    
    private Line2D walkUp( Line2D line, List<Point2D> left, List<Point2D> right ) {
    // This Method takes the line and "walks" it UP between the left and the 
    // right hull's.   
    //
    // Step 1: Check to see if the original line is the UPPER-tangent line to both hulls
    // Step 2: Walk UP the Left Hull
    // Step 3: Walk UP the Right Hull
    // Step 4: Check to see if this line is the UPPER-tangent to both hulls
    // 		   If not Tangent: Repeat Steps 2-4
    	
    	// Step 1:
        boolean isTangent = ( isTangent( line, left, LEFT, TOP ) 
                && isTangent( line, right, RIGHT, TOP ));
        
        while( !isTangent ) {
        	
        	// Step 2:
            while( !isTangent( line, left, LEFT, TOP )) {
                line = stepUp( line, left, LEFT );
            }
            
            // Step 3:
            while( !isTangent( line, right, RIGHT, TOP ) ) {
                line = stepUp( line, right, RIGHT );
            }
            
            // Step 4:
            isTangent = ( isTangent( line, left, LEFT, TOP ) 
                          && isTangent( line, right, RIGHT, TOP ));
        }
        
        // Return Upper Tangent Line
        return line;
    }
    
    private Line2D walkDown( Line2D line, List<Point2D> left, List<Point2D> right ) {
    // This Method takes the line and "walks" it DOWN between the left and the 
    // right hull's.   
    //
    // Step 1: Check to see if the original line is the LOWER-tangent line to both hulls
    // Step 2: Walk DOWN the Left Hull
    // Step 3: Walk DOWN the Right Hull
    // Step 4: Check to see if this line is the LOWER-tangent to both hulls
    // 		   If not Tangent: Repeat Steps 2-4	
    	
    	// Step 1:
        boolean isTangent = ( isTangent( line, left, LEFT, BOTTOM ) 
                && isTangent( line, right, RIGHT, BOTTOM ));
 
        while( !isTangent ) {
        	
        	// Step 2:
	        while( !isTangent( line, left, LEFT, BOTTOM )) {
	        	line = stepDown( line, left, LEFT );
	        }
	        
	        // Step 3:
	        while( !isTangent( line, right, RIGHT, BOTTOM ) ) {
	        	line = stepDown( line, right, RIGHT );
	        }
	        
	        // Step 4:
	        isTangent = ( isTangent( line, left, LEFT, BOTTOM ) 
	                    && isTangent( line, right, RIGHT, BOTTOM ));
        }
        
        // Return Bottom Tangent Line
        return line;
    }
    
    private Line2D stepUp( Line2D line, List<Point2D> points, int hull ) {
    // This Method will return a new line that is touching the given hull on
    // the point above the point that it is currently touching.
    //	
    // Step 1: This checks to see if the Step UP will be on the right or left Hull
    // Step 2: Then get the index of the point of contact between the hull and the line.
    // Step 3: Get the point that is above the point of contact
    // Step 4: Create a new line using the point above instead of the point of contact.
    
    	// Local Variable
    	Line2D returnLine = line;
        
    	// Step 1:
        if( hull == RIGHT ) {
        	
        	// Step 2:
            int i = points.lastIndexOf( line.getP2() );
            
            // Step 3:
            Point2D pointAbove = points.get(indexAbove( i, points, hull ));
            
            // Step 4:
            returnLine = new Line2D.Double( line.getP1(), pointAbove);
        }
        else { // LEFT HULL
        	
        	// Step 2:
            int i = points.lastIndexOf( line.getP1() );
            
            // Step 3: 
            Point2D pointAbove = points.get(indexAbove( i, points, hull ));
            
            // Step 4:
            returnLine = new Line2D.Double( pointAbove, line.getP2() );
        }
        
        // Return Line
        return returnLine;
    }
    
    private Line2D stepDown( Line2D line, List<Point2D> points, int hull ) {
	// This Method will return a new line that is touching the given hull on
    // the point below the point that it is currently touching.
    //	
    // Step 1: Check to see if the Step DOWN will be on the right or left Hull
    // Step 2: Then get the index of the point of contact between the hull and the line.
    // Step 3: Get the point that is below the point of contact
    // Step 4: Create a new line using the point below instead of the point of contact.
        	
    	// Local Variable
        Line2D returnLine = line;
        
        // Step 1: 
        if( hull == RIGHT ) { 
        	
        	// Step 2:
            int i = points.lastIndexOf( line.getP2() );
            
            // Step 3:
            Point2D pointBelow = points.get(indexBelow( i, points, hull ));
            
            // Step 4:
            returnLine = new Line2D.Double( line.getP1(), pointBelow);
        }
        else { // LEFT HULL
            
        	// Step 2:
        	int i = points.lastIndexOf( line.getP1() );
        	
        	// Step 3:
            Point2D pointBelow = points.get(indexBelow( i, points, hull ));
            
            // Step 4:
            returnLine = new Line2D.Double( pointBelow, line.getP2() );
        }
        
        // Return Line
        return returnLine;
    }
    
    private boolean isTangent( Line2D line, List<Point2D> points, int hull, int hullSideY ) {
    // This method returns a boolean value indicating whether or not the given line
    // is tangent to the given hull (list of points). The 'hull' parameter specifies
    // whether the line is touching the left or right hull, and the 'hullSideY' parameter
    // specifies whether the line is touching the top or bottom of the hull.
	//
	//Step 1: determine whether the line is touching on the left or the right.
	//Step 2: get the points above and below the point of contact
	//Step 3: determine if these points are on the correct side of the line
	//        to validate whether the line is tangent.
	//
	//Note: The gui that is currently being used to manipulate this class often
	//      generates points that share a common x value when generating about
	//      400 or more points on standard window size.  Conditional statements
	//      have been added to handle some, but not all of these cases. Problems
    //      may occur if pointAbove and pointBelow have the same x value as both
    //      points in the line
    //
    //Case A: Both pointAbove and PointBelow have the same x value as both of
    //        the points in the line.  This is currently the case that is causing
    //        problems.
	//
	//Cases B and C: Either the point above or the point below has the same x
	//               value as both points in the line.  In these cases, the 
    //               other point is then checked to see if it is on the correct 
    //               side of the line, depending on whether or not the line is 
    //               touching the hull on the top or bottom.
    //
    //Case D: Both points must be checked.
    
        boolean isTangent = true;
        int i;
        
        // Step 1:
        if( hull == RIGHT ) {
            i = points.lastIndexOf( line.getP2() );
        }
        else {//hull is on the left
            i = points.lastIndexOf( line.getP1() );
        }
        
        // Step 2:
        Point2D pointAbove = points.get(indexAbove( i, points, hull ));
        Point2D pointBelow = points.get(indexBelow( i, points, hull ));
        
        // Step 3:
        if( sameX( pointAbove, line) && sameX( pointBelow, line)) {
        	//this is does not correctly return a convex hull, but
        	//it prevents an infinite loop for the sake of convenience
        	isTangent = true;
        }
        // Case B:
        if( sameX( pointAbove, line ) ) {
        	if( hullSideY == TOP ) {
 	            if( line.relativeCCW(pointBelow) == 1 )
 	                isTangent = false;
 	        }
 	        else { //hullsideY == LOWER
 	            if( line.relativeCCW(pointBelow) == -1 )
 	                isTangent = false;
 	        }
        }
        // Case C:
        else if( sameX( pointBelow, line ) ) {
        	if( hullSideY == TOP ) {
 	            if( line.relativeCCW(pointAbove) == 1 )
 	                isTangent = false;
 	        }
 	        else { //hullsideY == LOWER
 	            if( line.relativeCCW(pointAbove) == -1 )
 	                isTangent = false;
 	        }
        }
        // Case D:
        else {
	        if( hullSideY == TOP ) {
	            if( line.relativeCCW(pointAbove) == 1 
	                    || line.relativeCCW(pointBelow) == 1 )
	                isTangent = false;
	        }
	        else { //is lower tangent
	            if( line.relativeCCW(pointAbove) == -1 
	                    || line.relativeCCW(pointBelow) == -1 )
	                isTangent = false;
	        }
        }
        
        return isTangent;
    }
    
    private boolean sameX( Point2D point, Line2D line ) {
    // This method was implemented to handle cases described in the
    // isTangent method. It returns a boolean value indicating whether
    // the given point has the same x value as both of the points that
    // make up the given line.
    	
    	// Local Variables
    	boolean colinear = false;    	
    	double x = point.getX();
    	
    	if( line.getP1().getX() == x && line.getP2().getX() == x ) {
    		colinear = true;
    	}
    	
    	return colinear;
    }
    
    private int indexAbove( int index, List<Point2D> points, int hull ) {
    // This method returns the index of the point that is "above" the point
    // in the hull that has the index indicated in the 'index' parameter.
    // the 'hull' parameter indicates whether the line is touching the hull
    // on the right or the left, which determines the direction to move in
    // to get the point "above."
    	
    	// Local Variable
        int indexAbove = index;
       
        if( hull == RIGHT ) {
            if( index == 0 ){ 
            	indexAbove = points.size()-1;
            }
            else indexAbove = index - 1;
        }
        else { // LEFT HULL
            if( index == points.size()-1 ) {
            	indexAbove = 0; 
            }
            else indexAbove = index + 1;
        }
        
        return indexAbove;
    }
    
    private int indexBelow( int index, List<Point2D> points, int hull ) {
    // This method is essentially the same as the indexAbove method, except
    // that it returns the index of the point below, instead of the point above.
        
    	// Local Variable;
    	int indexBelow = index;
        
        if( hull == RIGHT ) {
            if( index == points.size()-1 ){
            	indexBelow = 0;
            }
            else {
            	indexBelow = index + 1;
            }
        }
        else {  // LEFT HULL
            if( index == 0 ){
            	indexBelow = points.size()-1;
            }
            else { 
            	indexBelow = index - 1;
            }
        }
        
        return indexBelow;
    }
    
    private Point2D leftMostPoint( List<Point2D> points ) {
    // This method returns the point in the given list of points
    // that has the smallest x value.
    	
    	// Starting Point
        Point2D leftMostPoint = points.get(0);
	        
        // Search for a point with a smaller x value
        for( int i=0; i<points.size(); i++ ) {
        	if( points.get(i).getX() < leftMostPoint.getX() ) {
        		leftMostPoint = points.get(i);
        	}
        }
        
        // Return Point
        return leftMostPoint;
    }
    
    private Point2D rightMostPoint( List<Point2D> points ) {
    // This method returns the point in the given list of points
    // that has the greatest x value.
    		
    	// Starting Point
        Point2D rightMostPoint = points.get(0);
        
        // Search for a point with a greater x value
        for( int i=0; i<points.size(); i++ ) {
        	if( points.get(i).getX() > rightMostPoint.getX() ) {
        		rightMostPoint = points.get(i);
        	}
        }
        
        // Return Point
        return rightMostPoint;
    }
    

    private List<Point2D> ccw( List<Point2D> points ) {
    // This is a very important method. 
    // This method takes the base case of the X-Sorted Points
    // And sorts them in their proper Counter ClockWise Order
    
    	// Local Variable
    	List<Point2D> returnList = new ArrayList<Point2D>();
    	
    	if( points.size() == 3 ) {
    		// Create a Line from the first two Points
    		Line2D line = new Line2D.Double( points.get(0), points.get(1) );
    		
    		// Check to see if the 3rd point is CCW
    		if( line.relativeCCW( points.get(2)) > -1 ) {
    			// Already in CCW Order
    			returnList = points;
    		}
    		else{
    			// Switch around points 1 & 2
    			returnList.add( points.get(0) );
    			returnList.add( points.get(2) );
    			returnList.add( points.get(1) );
    		}
    	} else {
    		// If there is only 1 or 2 points, then it is in CCW-Order no matter what
    		returnList = points;
    	}
    	
    	// Return List
    	return returnList;
    }
}