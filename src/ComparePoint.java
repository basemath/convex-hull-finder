/**
 * @author David Bogle
 * @author John  Rankin
 */

import java.awt.geom.Point2D;
import java.util.Comparator;

public class ComparePoint implements Comparator<Point2D> {

    /**
     * This method is an implementation of compare from
     * the Comparator interface.  It is used to help sort
     * by certain attributes of an object using Collection.sort().
     * 
     * @param Point1
     * @param Point2
     * @return an int of either -1, 0, or 1 depending on which is larger.
     */
	
	public int compare(Point2D arg0, Point2D arg1) {
	// This method simply makes Point2D enabled to be sorted by the X Value.
		int compareTo = 0;
	       
		if( arg0.getX() < arg1.getX() )
			compareTo = -1;
		else if ( arg0.getX() > arg1.getX() )
			compareTo = 1;
	        
		return compareTo;
	}

}
