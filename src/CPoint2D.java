import java.awt.geom.Point2D;


public class CPoint2D extends Point2D implements Comparable<Point2D>
{
    
    /**
     * The X coordinate of this <code>Point2D</code>.
     * @since 1.2
     */
    public double x;

    /**
     * The Y coordinate of this <code>Point2D</code>.
     * @since 1.2
     */
    public double y;

    /**
     * Constructs and initializes a <code>Point2D</code> with
         * coordinates (0,&nbsp;0).
     * @since 1.2
     */
    public CPoint2D() {
    }

    /**
     * Constructs and initializes a <code>Point2D</code> with the
         * specified coordinates.
         * @param x,&nbsp;y the coordinates to which to set the newly
         * constructed <code>Point2D</code>
     * @since 1.2
     */
    public CPoint2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Returns the X coordinate of this <code>Point2D</code> 
         * in <code>double</code> precision.
         * @return the X coordinate of this <code>Point2D</code>.
     * @since 1.2
     */
    public double getX() {
        return x;
    }

    /**
     * Returns the Y coordinate of this <code>Point2D</code> in 
         * <code>double</code> precision.
         * @return the Y coordinate of this <code>Point2D</code>.
     * @since 1.2
     */
    public double getY() {
        return y;
    }

    /**
     * Sets the location of this <code>Point2D</code> to the 
         * specified <code>double</code> coordinates.
         * @param x,&nbsp;y the coordinates to which to set this
         * <code>Point2D</code>
     * @since 1.2
     */
    public void setLocation(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Returns a <code>String</code> that represents the value 
         * of this <code>Point2D</code>.
         * @return a string representation of this <code>Point2D</code>.
     * @since 1.2
     */
    public String toString() {
        return "Point2D.Double["+x+", "+y+"]";
    }
    
    public int compareTo( Point2D o )
    {
        int compareTo = 0;
        
        if( this.getX() < o.getX() )
            compareTo = -1;
        else if( this.getX() > o.getX() )
            compareTo = 1;
        
        
        return compareTo;
    }
}