/**
 * @author David Bogle
 * @author John Rankin
 */

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import java.util.ArrayList;
import java.util.List;

public class QuickHull implements ConvexHullFinder {
    
    //constants
    private static final boolean CCW = true;
    private static final boolean CW  = false;
    
    /**
     * This method is an implementation of computeHull from
     * the ConvexHullFinder interface.  It uses the quickhull
     * algorithm to computer the hull.
     * 
     * @param listOfPoints a List of Point2D around which the
     * hull is being computed.
     * @return a list of Point2D that composes the hull in 
     * clockwise order.
     */
    public List<Point2D> computeHull(List<Point2D> listOfPoints) {
        
    	// Variables 
        List<Point2D> returnHull = new ArrayList<Point2D>();
    	
		// Initial Setup:
		// Step 1: Find the outer most Points
		// Step 2: Draw a Line connecting outer most points
		// Step 3: Divide the points from above and below the Line
		// Step 4: Call The Recursive Method for above, and below the lines
		// Step 5: Connect Top Half and Bottom Half Together;
        
        //this check prevents errors with an empty list
        if( listOfPoints.size() > 0 ) {
            
        // Step 1:
            Point2D pointA = listOfPoints.get(0);
            Point2D pointB = listOfPoints.get(0);
        	for( int i=0; i<listOfPoints.size(); i++ )  {
        	    if( listOfPoints.get(i).getX() < pointA.getX() )
        	        pointA = listOfPoints.get(i);
        	    else if( listOfPoints.get(i).getX() > pointB.getX() )
        	        pointB = listOfPoints.get(i);
        	}
           
        // Step 2:
            Line2D lineAB = new Line2D.Double( pointA, pointB );

        // Step 3:
            List<Point2D> upperPoints = dividePoints(listOfPoints, lineAB, CCW);
            List<Point2D> lowerPoints = dividePoints(listOfPoints, lineAB, CW);

       // Step 4:
            List<Point2D> upperHull = recursiveComputeHull( upperPoints, lineAB );
            
            // For symmetry, we reverse line AB before passing it in with the lower Hull
            Line2D lineBA = new Line2D.Double( lineAB.getP2(), lineAB.getP1() );
            List<Point2D> lowerHull = recursiveComputeHull( lowerPoints, lineBA );

       // Step 5:
            returnHull = combineHulls( upperHull, lowerHull, lineAB ); 
        }

        return returnHull;
    }

    /**
     * This is the recursive method used in the quickHullAlgorithm
     * 
     * @param pointList the list of points being considered
     * @param lineAB considered as the base line (a to b)
     * in the QuickHull algorithm.
     * @return a list of points in clockwise order that compose
     * this section of the hull
     */   
    private List<Point2D> recursiveComputeHull(List<Point2D> pointList, Line2D lineAB) {
    	// Recursion Method:
    	// Step 1:  Divide the problem into smaller subproblems
    	// Step 2:  Conquer the subproblems
    	// Step 3:  Combine the results
    	
        List<Point2D> returnList = new ArrayList<Point2D>();
       
        // Base Case:
        // If this condition is true, the the points given
        // (either 0 points or 1 point ) are part of the hull,
        // since it is already assumed that the point is outside of
        // line AB, so we just add the pointList and return. 
        if( pointList.size() < 2 ) {
            returnList.addAll( pointList );
        }
        else {
        // Step 1: 
        	
        	// Some initial computation regarding finding the farthest point on lineAB
            Point2D pointA = lineAB.getP1();
            Point2D pointB = lineAB.getP2();
            Point2D pointC = farthestPoint( pointList, lineAB );

            // Construction of the Two lines Using the farthest Point from the Original
            // line.  The order of the points in the construction of the lines
            // is important, we reverse the points for the second line. 
            Line2D  lineAC = new Line2D.Double( pointA, pointC );
            Line2D  lineCB = new Line2D.Double( pointC, pointB );

          // leftPoints  is all the points outside of lineAC
          // rightPoints is all the points outside of lineCB
          List<Point2D> leftPoints = new ArrayList<Point2D>();
          List<Point2D> rightPoints = new ArrayList<Point2D>();
          
          // Find all points for leftPoints and rightPoints, with respect to 
          // relative counter-clockwise position
            for( int i=0; i<pointList.size(); i++ ) {
            	if( lineAC.relativeCCW(pointList.get(i)) == 1  ) {
            			leftPoints.add(pointList.get(i));
            	}
                else if ( lineCB.relativeCCW(pointList.get( i )) == 1 ) {
                    rightPoints.add( pointList.get(i));
                }
            }
        // Step 2 &  Step 3:
        // Since The recursive method is returning a list, it is much simpler to just call the
        // recursive method into an AddAll method, so the final result will end up with Points
        // in just 1 List.
            // In order to preserve and maintain the correct order of the hull, 
            // we must add the results, in the order of:
            // rightPoints , Point C, LeftPoints.
            returnList.addAll( recursiveComputeHull( rightPoints, lineCB ));
            returnList.add( pointC );
            returnList.addAll( recursiveComputeHull( leftPoints, lineAC ));
        }

        // Return the list
        return returnList;
    }

    /**
     * This is used by the quickhull algorithm to find point C.
     * 
     * @param points a list of points being searched through
     * for the farthest point among them
     * @param line the line from which the farthest point is
     * being found
     * @return the farthest point amidst the list of points from
     * the given line.  
     */
    private Point2D farthestPoint(List<Point2D> points, Line2D line) {
       // Local Variables
       Point2D farthestPoint;

       if( points.size() > 0 ) {
    	   // Starting Point
           farthestPoint = points.get(0);
        
           // Compare all points and find out exactly which point has 
           // the greatest distance with respect to the line.           
           for( int i=0; i<points.size(); i++ ){
        	   if( line.ptLineDist( points.get(i) ) > line.ptLineDist( farthestPoint ))
                   farthestPoint = points.get(i);
           }
       } else {
    	   // If there are no points, because the points.size() == 0, then 
    	   // the farthest point will return null.
    	   farthestPoint = null;
       }
       
       // Return farthestPoint
       return farthestPoint;
    }

    /**
     * This method is used to combine everything
     * after the upper hull and lower hull are computed.
     * 
     * @param upper the upper hull (as a list of Point2D)
     * @param lower the lower hull (as a list of Point2D)
     * @param baseLine the original lineAB in the quickhull
     * algorithm
     * @return the final hull (as a list of Point2D) that is
     * composed of point B, the upper hull points, point A,
     * and the lower hull points in CCW order.
     */
    private List<Point2D> combineHulls(List<Point2D> upper, List<Point2D> lower, Line2D baseLine) {
    	// Local Variables
        List<Point2D> returnList = new ArrayList<Point2D>();

        // Order is extremely important here.
        
        // Add the second point from the base line (point B)
        returnList.add( baseLine.getP2() );

        // Add the upper hull points
        returnList.addAll( upper );
        
        // Add the first point from the base line (point A)
        returnList.add( baseLine.getP1() );

        // Add the lower hull points
        returnList.addAll( lower );

        // Return the final combination
        return returnList;
    }

    /**
     * @param points the list of points being divided
     * @param line the line by which the points are being divided
     * @param CCW true if the points desired are CCW relative to the line
     * @return all points that are either CCW or CW to the line
     */
    private List<Point2D> dividePoints(List<Point2D> points, Line2D line, boolean CCW) {
    // Takes in a boolean value of CCW, to know if the points need to be above or below the line
    // And checks the points to see see if they will be added to the list.   
    // If the points are on the same line as the line,
    // They will not be Added.
    	
    	// Local Variables
		List<Point2D> result = new ArrayList<Point2D>();
		
		for (int i = 0; i < points.size(); i++) {
			if (CCW) {
				if (line.relativeCCW(points.get(i)) > 0)
					result.add(points.get(i));
			} else {
				if (line.relativeCCW(points.get(i)) < 0)
					result.add(points.get(i));
			}
		}
		
		// Return List
		return result;
	}

}