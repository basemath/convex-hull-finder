This was a lab that I completed with a partner for my algorithms class.  
It's old code and isn't my proudest piece of work, but it's interesting nonetheless.  
  
There are two classes that implement different convex hull finding algorithms with complexities approximately at log(n)*n. A GUI interface is provided to demonstrate the program.
